const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const routes = require("./server/routes");
const cors = require('cors')
const app = express();
const mongoose = require('mongoose')
const db = process.env.MONGO_URI
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Successfully connected to DB'))
    .catch(err => console.log('Sorry connection error'))
app.use(logger("dev"));
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
routes(app);
// require('./db/connection');
require('./server/db')
app.get("*", (req, res) =>
    res.status(200).send({ message: "Welcome to the ENTERTWINE API" })
);

module.exports = app;
