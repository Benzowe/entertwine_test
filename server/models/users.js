'use strict';

const Sequelize = require('sequelize')
const db = require('../db/index')

const User = db.define('user', {
    name: {
        type: Sequelize.STRING

    },
    email: {
        type: Sequelize.STRING,
    },
    password: {
        type: Sequelize.STRING
    },
    phone: {
        type: Sequelize.NUMBER
    },
    is_admin: {
        type: Sequelize.BOOLEAN
    }


},
    {
        freezeTableName: true,
        timestamps: false,
        underscored: true,
    }
)
module.exports = User;