require('dotenv').config();
const Sequelize = require('sequelize')
const sequelize = new Sequelize(process.env.URL_STRING);

sequelize
    .authenticate()
    .then(() => {
        console.log("Sequelize is running");
    }).catch((err) => {
        console.log("Sequelize cannot connect" + err);
    });





module.exports = sequelize
global.sequelize = sequelize;