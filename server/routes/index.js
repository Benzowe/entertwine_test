const Users = require('../controllers/users')
const user_auth = require('../services/authService')

module.exports = app => {
    //users Route
    app.post('/api/v1/users/create', Users.signUp)
    app.post('/api/v1/users/login', Users.signIn)
}