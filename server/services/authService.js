const User = require('../models/users')
const jwt = require('jsonwebtoken')

const checkUser = (req, res, next) => {
    let token = req.headers['authorization'] || req.headers['x-access-token']
    if (!token) {
        return res.status(400).json({
            success: false,
            message: 'Please add token'
        })
    }
    if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length)
    }
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
            if (err) {
                return res.status(401).json({
                    success: false,
                    message: 'Unauthorized'
                })
            }
            const { email } = decoded
            return User.findOne({ email })
                .then(user => {
                    if (user) {
                        req.user = user
                        next()
                    }
                    else {
                        return res.status(401).json({
                            success: false,
                            message: 'Unauthorized'
                        })
                    }
                })
                .catch(err => {
                    return res.status(400).json({
                        success: false,
                        message: 'Sorry, an error occured'
                    })
                })
        })
    }

}

module.exports = checkUser