const https = require('https');
require('dotenv').config();
const webHookURL = process.env.SLACK_WEBHOOK;

/**
 * Handles the actual sending request. 
 * We're turning the https.request into a promise here for convenience
 * @param webhookURL
 * @param messageBody
 * @return {Promise}
 */
function sendSlackMessage(webhookURL, messageBody) {
  // make sure the incoming message body can be parsed into valid JSON
  try {
    messageBody = JSON.stringify(messageBody);
  } catch (e) {
    throw new Error('Failed to stringify messageBody', e);
  }

  // Promisify the https.request
  return new Promise((resolve, reject) => {
    // general request options, we defined that it's a POST request and content is JSON
    const requestOptions = {
      method: 'POST',
      header: {
        'Content-Type': 'application/json'
      }
    };

    // actual request
    const req = https.request(webhookURL, requestOptions, (res) => {
      let response = '';


      res.on('data', (d) => {
        response += d;
      });

      // response finished, resolve the promise with data
      res.on('end', () => {
        resolve(response);
      })
    });

    // there was an error, reject the promise
    req.on('error', (e) => {
      reject(e);
    });

    // send our message body (was parsed to JSON beforehand)
    req.write(messageBody);
    req.end();
  });
}

// main
async function sendMessage(name, email) {
  if (!webHookURL) {
    console.log('Please fill in your Webhook URL');
  }

  console.log('Sending slack message');
  try {
    let userAccountNotification = {
      'username': 'Error notifier', // This will appear as user name who posts the message
      'text': ' Unable to get data from primary data source.', // text
      'icon_emoji': ':bangbang:', // User icon, you can also use custom icons here
      'attachments': [{ // this defines the attachment block, allows for better layout usage
        'color': '#eed140', // color of the attachments sidebar.
        'fields': [ // actual fields
          {
            'title': 'Environment', // Custom field
            'value': 'Development', // Custom value
            'short': true // long fields will be full width
          },
          {
            'title': 'Database',
            'value': 'User database',
            'short': true
          },
          {
            'title': 'New User Added', // Custom field
            'value': name, // Custom value
            'short': true
          },
          {
            'title': 'Email Address', // Custom field
            'value': email, // Custom value
            'short': true // long fields will be full width
          }
        ]
      }]
    };
    const slackResponse = await sendSlackMessage(webHookURL, userAccountNotification);
    console.log('Message response', slackResponse);
  } catch (e) {
    console.log('There was a error with the request', e);
  }
};

module.exports = sendMessage;