require('dotenv').config();
const User = require('../models/users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validateRegisterInput = require('../validations/register');
const sendMessage = require('../services/slackIntegration');

class Users {
	static signUp(req, res) {
		const { name, email, password, phone, is_admin } = req.body;
		if (!email || !password || !name || !phone) {
			return res.status(400).json({
				success: false,
				message: 'Please fill in all credentials'
			})
		}
		const { errors, isValid } = validateRegisterInput(req.body)
		if (!isValid) {
			return res.status(400).json(errors)
		}

		return User.findOne({
			where: { email: email }
		})
			.then(result => {
				if (!result) {
					bcrypt.hash(req.body.password, 10)
						.then(hash => {
							req.body.password = hash;
							return User.create({
								name,
								email,
								phone,
								password: hash,
								is_admin: false
							})
								.then(result => {
									const payload = result
									jwt.sign(
										payload,
										process.env.SECRET_KEY,
										{ expiresIn: 3600 },
										(err, token) => {
											//Sends message to slack about a new user signing up
											//ensure to add slack webhook url to env
											sendMessage(name, email)

											return res.status(200).send({
												success: true,
												message: 'Signed Up Successfully',
												status: 200,
												data: {
													name, email, phone
												}
											});
										}
									)
								})
								.catch(err => {
									console.log(err)
									return res.status(400).json({
										success: false,
										message: 'Sorry, User could not be created'
									})
								})
						})
				}
				else {
					errors.email = 'Email Already Exists'
					return res.status(400).json(errors)
				}
			})
			.catch(err => {
				console.log(err)
				return res.status(400).json({
					success: false,
					message: 'Sorry,an error occured'
				})
			})


	}
	static signIn(req, res) {
		const { email, password } = req.body;
		const user = { email, password }

		if (!user.email) {
			res.status(400).json({
				status: "Error",
				error: "Email not supplied"
			});
			return;
		}

		if (!user.password) {
			res.status(400).json({
				status: "Error",
				error: "Password not supplied"
			});
			return;
		}

		return User.findOne({ where: { email: user.email } })
			.then(async result => {
				if (!result) {
					return res.json({
						success: false,
						message: "Sorry, No User Found"
					})
				}

				await bcrypt.compare(user.password, result.password)
					.then(async response => {
						if (response == true) {
							const payload = {
								id: response.id,
								name: response.name

							};
							await jwt.sign(
								payload,
								process.env.SECRET_KEY,
								{ expiresIn: 3600 },
								(err, token) => {
									return res.status(200).send({
										success: true,
										message: 'Signed In Successfully',
										status: 200,
										token: token,
										data: {
											id: result.id,
											name: result.name,
											email: result.email
										}
									});
								}
							);
						}
						else {
							return res.status(400).json({
								success: false,
								message: 'Sorry, Invalid Credentials'
							})
						}
					})
					.catch(err => {
						return res.status(400).json({
							success: false,
							message: 'Sorry,an error occured'
						})
					})
			})
			.catch(err => {
				return res.status(400).json({
					success: false,
					message: 'Sorry,an error occured'
				})
			})
	}
}

module.exports = Users;